package com.ingress.teacherms14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacherMs14Application {

    public static void main(String[] args) {
        SpringApplication.run(TeacherMs14Application.class, args);
    }

}
