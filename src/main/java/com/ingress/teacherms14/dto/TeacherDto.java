package com.ingress.teacherms14.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TeacherDto {
    private int id;

    private String name;
    private String faculty;
    private LocalDate birthdate;
}
