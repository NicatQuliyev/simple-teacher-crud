package com.ingress.teacherms14.controller;

import com.ingress.teacherms14.dto.TeacherDto;
import com.ingress.teacherms14.entity.Teacher;
import com.ingress.teacherms14.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/teachers")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping
    public List<TeacherDto> findAll() {
        return teacherService.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Teacher> findById(@PathVariable Integer id) {
        return teacherService.findById(id);
    }

    @PostMapping
    public void save(@RequestBody Teacher teacher) {
        teacherService.create(teacher);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Integer id, @RequestBody Teacher teacher) {
         teacherService.update(id, teacher);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        teacherService.delete(id);
    }
}
