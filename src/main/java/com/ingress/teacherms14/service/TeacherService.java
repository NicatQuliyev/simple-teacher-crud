package com.ingress.teacherms14.service;

import com.ingress.teacherms14.dto.TeacherDto;
import com.ingress.teacherms14.entity.Teacher;
import com.ingress.teacherms14.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TeacherService {

    private final TeacherRepository teacherRepository;

    private final ModelMapper modelMapper;

    public List<TeacherDto> findAll() {
        return teacherRepository
                .findAll()
                .stream()
                .map(teacher -> modelMapper.map(teacher, TeacherDto.class))
                .collect(Collectors.toList());
    }

    public Optional<Teacher> findById(Integer id) {
        return Optional.of(teacherRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("There is not Teacher by id " + id)));
    }

    public void create(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public void update(Integer id, Teacher teacher) {
        Optional<Teacher> teacher1 = teacherRepository.findById(id);

        teacher1.ifPresent(teacher2 -> {
            teacher2.setName(teacher.getName());
            teacher2.setBirthdate(teacher.getBirthdate());
            teacher2.setFaculty(teacher.getFaculty());

            teacherRepository.save(teacher2);
        });
    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }
}
